/*override                                                                                                       
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.electricity.billing.service.impl;

import in.ac.gpckasaragod.electricity.billing.model.ui.ElectricityBIllingUiModel;
import in.ac.gpckasaragod.electricity.billing.service.ElectricityBillingService;
import in.ac.gpckasaragod.electricity.billing.ui.model.data.ElectricityBilling;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */     
public class ElectricityBillingImpl extends ConnectionServiceImpl implements ElectricityBillingService {

    private String unitchargeid;
    
    public String saveElectricityBilling(String costumername,Double unit,Double totalcharge,Integer costumerno,Integer unitchargeid) {
        //throw new UnsupportedOperationException("Not supported yet."); //Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
      try {     
        
             Connection connection=getConnection();
  
             Statement statement =connection.createStatement();
             String query ="INSERT INTO ELECTRICITY_BILLING (COSTUMER_NAME,UNIT,TOTAL_CHARGE,COSTUMER_NO,UNIT_CHARGE_ID) VALUES"
                     + "('" + costumername + "','" + unit + "','" + totalcharge + "','" + costumerno + "','" + unitchargeid + "')";
                     System.err.println("Query:" + query);
                     int status = statement.executeUpdate(query);
                     if(status != 1) {
                         return "save failed";
                       
                     }else {
                         return "saved successfully";
                         
                     }
    }catch (SQLException ex) {
            Logger.getLogger(ElectricityBillingImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "save failed";

    }
   
    }
    @Override
    public ElectricityBilling readElectricityBilling(Integer id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
     
        ElectricityBilling electricityBilling;
        electricityBilling = null;
        
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM ELECTRICITY_BILLING WHERE ID=" + id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()) {
                id = resultSet.getInt("ID");
                 Integer unitchargeid = resultSet.getInt("UNIT_CHARGE_ID");
                String costumername = resultSet.getString("COSTUMER_NAME");
                Double unit = resultSet.getDouble("UNIT");
                Double totalcharge = resultSet.getDouble("TOTAL_CHARGE");
                Integer costumerno = resultSet.getInt("COSTUMER_NO");
               
                electricityBilling = new ElectricityBilling (id,costumername,unit,totalcharge,costumerno,unitchargeid);
                
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ElectricityBillingImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return electricityBilling;
            
        
        
     
           
    }

    @Override
    public List<ElectricityBIllingUiModel> getALLElectricityBilling() {
         List<ElectricityBIllingUiModel> unitchargedetailss = new ArrayList<>();
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT E.ID,E.COSTUMER_NAME,E.COSTUMER_NO,E.UNIT,E.TOTAL_CHARGE,E.UNIT_CHARGE_ID,U.FROM_UNIT,U.TO_UNIT,U.CHARGE ,U.SURCHARGE FROM ELECTRICITY_BILLING E JOIN UNIT_CHARGE_DETAILS U ON E.UNIT_CHARGE_ID=U.ID";
            ResultSet resultSet = statement.executeQuery(query);
            
            while(resultSet.next()) {
                
               
                Integer id = resultSet.getInt("ID");
                Integer unitchargeid = resultSet.getInt("UNIT_CHARGE_ID");
                String costumername = resultSet.getString("COSTUMER_NAME");
                Double unit = resultSet.getDouble("UNIT");
                Double totalcharge = resultSet.getDouble("TOTAL_CHARGE");
                Integer costumerno = resultSet.getInt("COSTUMER_NO");
                
                
                Integer fromunit = resultSet.getInt("FROM_UNIT");
                Integer tounit = resultSet.getInt("TO_UNIT");
                Double charge = resultSet.getDouble("CHARGE");
                Double surcharge = resultSet.getDouble("SURCHARGE");
            ElectricityBIllingUiModel electricityBIllingUiModel     = new ElectricityBIllingUiModel (costumername,unit,totalcharge,costumerno,unitchargeid,id,fromunit,tounit,charge,surcharge);
                
                
               unitchargedetailss.add(electricityBIllingUiModel);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ElectricityBillingImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return   unitchargedetailss;
    }

    public String updateElectricityBilling(Integer id,String costumername,Double unit,Double totalcharge,Integer costumerno,Integer unitchargeid)  {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE ELECTRICITY_BILLING SET COSTUMER_NAME='"+costumername+"',UNIT='"+unit+"',TOTAL_CHARGE='"+totalcharge+"',COSTUMER_NO='"+costumerno+"' where ID='"+id+"'";
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if(update !=1)
                return "Update failed";
            else
                return "Update successfully";
        } catch (SQLException ex) {
            return "Update failed";
        }
           
        
       
        

        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
       
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    


    }

    @Override
    public String deleteElectricityBilling(Integer id) {
        try {
            Connection connection = getConnection();
            String query= "DELETE FROM ELECTRICITY_BILLING WHERE ID=?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1,id);
            int delete =statement.executeUpdate();
            if(delete !=1)
                return"Delete failed";
            else
                return "deleted successfully";
        } catch (SQLException ex) {
            Logger.getLogger(ElectricityBillingImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Delete failed";
        
    }

   
   
   

    

 

    
    

  
    }

  

    

    
    

