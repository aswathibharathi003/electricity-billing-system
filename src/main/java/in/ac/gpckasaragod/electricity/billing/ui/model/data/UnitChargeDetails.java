/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.electricity.billing.ui.model.data;

/**
 *
 * @author student
 */
public class UnitChargeDetails {

    private Integer id;
    private Integer fromUnit;
    private Integer toUnit;
    private Double charge;
    private Double surcharge;

    public UnitChargeDetails(Integer id, Integer fromUnit, Integer toUnit, Double charge, Double surcharge) {
        this.id = id;
        this.fromUnit = fromUnit;
        this.toUnit = toUnit;
        this.charge = charge;
        this.surcharge = surcharge;
    }

    public UnitChargeDetails(Integer id, String fromunit, String tounit, String charge, String surcharge) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFromUnit() {
        return fromUnit;
    }

    public void setFromUnit(Integer fromUnit) {
        this.fromUnit = fromUnit;
    }

    public Integer getToUnit() {
        return toUnit;
    }

    public void setToUnit(Integer toUnit) {
        this.toUnit = toUnit;
    }

    public Double getCharge() {
        return charge;
    }

    public void setCharge(Double charge) {
        this.charge = charge;
    }

    public Double getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(Double surcharge) {
        this.surcharge = surcharge;
    }


}
