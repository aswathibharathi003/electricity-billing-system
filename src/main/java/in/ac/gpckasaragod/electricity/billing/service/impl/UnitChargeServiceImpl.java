 /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.electricity.billing.service.impl;

import in.ac.gpckasaragod.electricity.billing.ui.model.data.UnitChargeDetails;
import java.util.List;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import in.ac.gpckasaragod.electricity.billing.service.UnitChargeService;
import java.sql.PreparedStatement;

/**
 *
 * @author student
 */
public class UnitChargeServiceImpl extends ConnectionServiceImpl implements UnitChargeService{
  
     @Override
    public String saveUnitChargeDetails(Integer fromunit, Integer tounit, Double charge, Double surcharge) {
       // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody

   
         try {
             Connection connection = getConnection();
             Statement statement = connection.createStatement();
             String query ="INSERT INTO UNIT_CHARGE_DETAILS (FROM_UNIT,TO_UNIT,CHARGE,SURCHARGE) VALUES"
                     +"("+fromunit+","+tounit+","+charge+","+surcharge+")";  
                     System.err.println("Query:"+query);
                     int status = statement.executeUpdate(query);
                     if(status !=1) {
                         return "save failed";
                         
                     }else {
                         return "saved succesfully";
                         
                     }
                     
                     }catch (SQLException ex){
                         Logger.getLogger(UnitChargeServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
                         return "save failed";
         }
     }
     @Override
     public UnitChargeDetails readUnitChargeDetails(Integer id) {
 
         
             UnitChargeDetails unitChargeDetails;
             unitChargeDetails = null;
             try{
             Connection connection = getConnection();
             Statement statement = connection.createStatement();
             String query = "SELECT * FROM UNIT_CHARGE_DETAILS WHERE ID="+id;
             ResultSet resultSet =statement.executeQuery(query);
             while(resultSet.next()){
             id = resultSet.getInt("ID");
             Integer fromunit = resultSet.getInt("FROM_UNIT");
             Integer tounit = resultSet.getInt("TO_UNIT");
             Double charge = resultSet.getDouble("CHARGE");
             Double surcharge = resultSet.getDouble("SURCHARGE");
             unitChargeDetails = new UnitChargeDetails(id, fromunit, tounit, charge, surcharge);
                 
             }
            
         }catch (SQLException ex){
             Logger.getLogger(UnitChargeServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            
                 
             }
             return unitChargeDetails;
       
             
         }
 
    @Override
      public List<UnitChargeDetails>getAllUNitChargeDetails() {
         List<UnitChargeDetails> unitChargeDetailss = new ArrayList<>();
         try{
         Connection connection = getConnection();
         Statement statement = connection.createStatement();
         String query = "SELECT * FROM UNIT_CHARGE_DETAILS";
         ResultSet resultSet = statement.executeQuery(query);
         
         while(resultSet.next()){
              Integer  id = resultSet.getInt("ID");
              Integer fromunit = resultSet.getInt("FROM_UNIT");
              Integer tounit = resultSet.getInt("TO_UNIT");
              Double charge = resultSet.getDouble("CHARGE");
              
      Double surcharge = resultSet.getDouble("SURCHARGE");
              UnitChargeDetails unitChargeDetails = new UnitChargeDetails(id,fromunit,tounit,charge,surcharge);
              unitChargeDetailss.add(unitChargeDetails);                                              
             }
     }catch(SQLException ex) {
     Logger.getLogger(UnitChargeServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
     }
         return unitChargeDetailss;
       }
     

    @Override
     public String updateUnitChargeDetails(int id,int fromunit,int tounit,Double charge,Double surcharge) {
         try{
             Connection connection = getConnection();
             Statement statement = connection.createStatement();
             String query = "UPDATE UNIT_CHARGE_DETAILS SET FROM_UNIT='"+fromunit+"',TO_UNIT='"+tounit+"',CHARGE='"+charge+"',SURCHARGE='"+surcharge+"'WHERE ID=" +id;
             System.out.print(query);
             int update = statement.executeUpdate(query);
             if(update !=1){
                 return "Update failed";
             } else {
                 return "Update successfully";
         }
         }catch(SQLException ex){
             Logger.getLogger(UnitChargeServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
         } 
             return "Update failed";
             
     }
   
     @Override
         public String deleteUnitChargeDetails(Integer id) {
         
      try{
          Connection connection = getConnection();
          String query = "DELETE FROM UNIT_CHARGE_DETAILS WHERE ID =?";
          PreparedStatement statement = connection.prepareCall(query);
          statement.setInt(1,id);
          int delete =statement.executeUpdate();
          if(delete !=1)
              return "Delete failed";
          else
              return"Deleted succesfully";
          
      } catch (SQLException ex) {
          Logger.getLogger(UnitChargeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
          
     }
      return "Delete failed";
     
         }

   
    @Override
    public String saveUnitChargeDetails(Integer fromunit, Integer id, Integer tounit, Double charge, Double surcharge) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    

    
  
}