/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.electricity.billing.ui.model.data;

/**
 *
 * @author student
 */
public class ElectricityBilling {

   private Integer Id;
    private String costumerName;
     private Double unit;
    private Double totalCharge;
    private Integer customerNumber;
    private Integer unitChargeId;

    public ElectricityBilling(Integer Id, String costumerName, Double unit, Double totalCharge, Integer customerNumber, Integer unitChargeId) {
        this.Id = Id;
        this.costumerName = costumerName;
        this.unit = unit;
        this.totalCharge = totalCharge;
        this.customerNumber = customerNumber;
        this.unitChargeId = unitChargeId;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getCostumerName() {
        return costumerName;
    }

    public void setCostumerName(String costumerName) {
        this.costumerName = costumerName;
    }

    public Double getUnit() {
        return unit;
    }

    public void setUnit(Double unit) {
        this.unit = unit;
    }

    public Double getTotalCharge() {
        return totalCharge;
    }

    public void setTotalCharge(Double totalCharge) {
        this.totalCharge = totalCharge;
    }

    public Integer getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(Integer customerNumber) {
        this.customerNumber = customerNumber;
    }

    public Integer getUnitChargeId() {
        return unitChargeId;
    }

    public void setUnitChargeId(Integer unitChargeId) {
        this.unitChargeId = unitChargeId;
    }

}