/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.electricity.billing.service;

import in.ac.gpckasaragod.electricity.billing.ui.model.data.UnitChargeDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface UnitChargeService {
    public String  saveUnitChargeDetails(Integer fromunit,Integer id,Integer tounit,Double charge,Double surcharge);
    public UnitChargeDetails readUnitChargeDetails(Integer id);
    public List<UnitChargeDetails>getAllUNitChargeDetails();
    public String updateUnitChargeDetails(int id,int fromunit,int tounit,Double charge,Double surcharge);
    public String deleteUnitChargeDetails(Integer id);

    public String saveUnitChargeDetails(Integer fromunit, Integer tounit, Double charge, Double surcharge);

}
