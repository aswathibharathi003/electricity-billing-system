/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.electricity.billing.service;

import in.ac.gpckasaragod.electricity.billing.model.ui.ElectricityBIllingUiModel;
import in.ac.gpckasaragod.electricity.billing.ui.model.data.ElectricityBilling;
import java.util.List;

/**
 *
 * @author student
 */
public interface ElectricityBillingService {
     public String saveElectricityBilling(String costumername,Double unit,Double totalcharge,Integer costumerno,Integer unitchargeid);
    public ElectricityBilling readElectricityBilling(Integer id);
    public List<ElectricityBIllingUiModel>getALLElectricityBilling();
    public String updateElectricityBilling(Integer id,String costumername,Double unit,Double totalcharge,Integer costumerno,Integer unitchargeid);
    public String deleteElectricityBilling(Integer id);

   

  
}
