/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.electricity.billing.model.ui;

/**
 *
 * @author student
 */
public class ElectricityBIllingUiModel {
    private String costumerName;
     private Double unit;
    private Double totalCharge;
    private Integer customerNumber;
    private Integer unitChargeId;

    
     private Integer id;
    private Integer fromUnit;
    private Integer toUnit;
    private Double charge;
    private Double surcharge;

    public ElectricityBIllingUiModel(String costumerName, Double unit, Double totalCharge, Integer customerNumber, Integer unitChargeId, Integer id, Integer fromUnit, Integer toUnit, Double charge, Double surcharge) {
        this.costumerName = costumerName;
        this.unit = unit;
        this.totalCharge = totalCharge;
        this.customerNumber = customerNumber;
        this.unitChargeId = unitChargeId;
        this.id = id;
        this.fromUnit = fromUnit;
        this.toUnit = toUnit;
        this.charge = charge;
        this.surcharge = surcharge;
    }

    public String getCostumerName() {
        return costumerName;
    }

    public void setCostumerName(String costumerName) {
        this.costumerName = costumerName;
    }

    public Double getUnit() {
        return unit;
    }

    public void setUnit(Double unit) {
        this.unit = unit;
    }

    public Double getTotalCharge() {
        return totalCharge;
    }

    public void setTotalCharge(Double totalCharge) {
        this.totalCharge = totalCharge;
    }

    public Integer getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(Integer customerNumber) {
        this.customerNumber = customerNumber;
    }

    public Integer getUnitChargeId() {
        return unitChargeId;
    }

    public void setUnitChargeId(Integer unitChargeId) {
        this.unitChargeId = unitChargeId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFromUnit() {
        return fromUnit;
    }

    public void setFromUnit(Integer fromUnit) {
        this.fromUnit = fromUnit;
    }

    public Integer getToUnit() {
        return toUnit;
    }

    public void setToUnit(Integer toUnit) {
        this.toUnit = toUnit;
    }

    public Double getCharge() {
        return charge;
    }

    public void setCharge(Double charge) {
        this.charge = charge;
    }

    public Double getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(Double surcharge) {
        this.surcharge = surcharge;
    }



   
    
}
